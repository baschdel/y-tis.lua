# X20 Node Programming Guide

## Basic syntax
A line may contain
- a label
- a command
- a comment

(LABEL:)(COMMAND)(;COMMENT)

## Labels
A label Is global if it does not start with a dot ".".
If a label starts with a dot it is internally turned into a global label
by appending a it to the last global label.

Example:
```asm
GLOBALLABEL:
;code here
.LOCALABEL: ;GLOBALLABEL.LOCALABEL
;more code here
JMP .LOCALABEL ;GLOBALLABEL.LOCALABEL
;code
GLOBAL2:
;code
.LOCALABEL: ;GLOBAL2.LOCALABEL
;code
JMP .LOCALABEL ;GLOBAL2.LOCALABEL
;code
JMP GLOBALLABEL.LOCALABEL
```

## Assembler directives

### #DEFINE key value

the #DEFINE assembler directive sets a key equal to some value, that can either be a number a label or a register.
The key then can be used in the code and will get replaced by whatever you defined it to be.

DEFINEs are NOT recursive!

### #ORG address

the #ORG directive accepts a number as argument and sets the writing "cursor" to the address given to it.
Meaning that the next instruction will be placed at the specified memory location

## Tables

### Registers

| ID | alias | alias | Description              |
| -- | ----- | ----- | ------------------------ |
| 00 | NIL   |       | Always 0                 |
| 01 | ACC   | R1    | General purpose register |
| 02 | R2    |       | General purpose register |
| 03 | R3    |       | General purpose register |
| 04 | R4    |       | General purpose register |
| 05 | R5    |       | General purpose register |
| 06 | RR6   |       | RESERVED                 |
| 07 | RR7   |       | RESERVED                 |
| 08 | I0    | O0    | Input/Output register    |
| 09 | I1    | O1    | Input/Output register    |
| 10 | I2    | O2    | Input/Output register    |
| 11 | I3    | O3    | Input/Output register    |
| 12 | I4    | O4    | Input/Output register    |
| 13 | I5    | O5    | Input/Output register    |
| 14 | I6    | O6    | Input/Output register    |
| 15 | I7    | O7    | Input/Output register    |

### Instructions

| Instruction | size | Name                 | Description                                                                          |
| ----------- | ---- | -------------------- | -------------------------------------------------------------------------------------|
| NOP         | 1    | no operation         | Will do nothing for the next tick                                                    |
| HLT         | 1    | halt                 | Will halt the processor until it is reset                                            |
| ADD R X Y   | 1    | add                  | reg(R) = reg(X) + reg(Y)                                                             |
| SUB R X Y   | 1    | substract            | reg(R) = reg(X) - reg(Y)                                                             |
| MUL R X Y   | 1    | multiply             | reg(R) = reg(X) * reg(Y)                                                             |
| DIV R X Y   | 1    | divide               | reg(R) = reg(X) / reg(Y)                                                             |
| AND R X Y   | 1    | and                  | reg(R) = reg(X) & reg(Y)                                                             |
| OR R X Y    | 1    | or                   | reg(R) = reg(X) or reg(Y)                                                            |
| XOR R X Y   | 1    | xor                  | reg(R) = reg(X) xor reg(Y)                                                           |
| SHF R X Y   | 1    | shift                | reg(R) = reg(X) << reg(Y)                                                            |
| ROL R X Y   | 1    | roll                 | reg(R) = reg(X) roll reg(Y)                                                          |
| ADD R X     | 1    | add                  | reg(R) = reg(R) + reg(X)                                                             |
| SUB R X     | 1    | substract            | reg(R) = reg(R) - reg(X)                                                             |
| MUL R X     | 1    | multiply             | reg(R) = reg(R) * reg(X)                                                             |
| DIV R X     | 1    | divide               | reg(R) = reg(R) / reg(X)                                                             |
| AND R X     | 1    | and                  | reg(R) = reg(R) & reg(X)                                                             |
| OR R X      | 1    | or                   | reg(R) = reg(R) or reg(X)                                                            |
| XOR R X     | 1    | xor                  | reg(R) = reg(R) xor reg(X)                                                           |
| SHF R X     | 1    | shift                | reg(R) = reg(R) << reg(X)                                                            |
| ROL R X     | 1    | roll                 | reg(R) = reg(R) roll reg(X)                                                          |
| JMP T C O   | 1    | conditional jump     | if reg(T) meets condition C jump to offset reg(O)                                    |
| JMP T C L O | 3    | conditional jump     | if reg(T) meets condition C jump to label(L) using reg(O) to store the offset        |
| AJMP A      | 1    | absolute jump        | jump to absolute address reg(A)                                                      |
| AJMP L A    | 3    | absolute jump        | jump to abolute label(L) using reg(A) to store the address                           |
| RJMP O      | 1    | relative jump        | jump to offset reg(O)                                                                |
| RJMP L O    | 3    | relative jump        | jump to label(L) using reg(O) to store the offset                                    |
| NEG R X     | 1    | negate               | reg(R) = -reg(X)                                                                     |
| NEG R       | 1    | negate               | reg(R) = -reg(R)                                                                     |
| NOT R X     | 1    | invert               | reg(R) = ~reg(X)                                                                     |
| NOT R       | 1    | invert               | reg(R) = ~reg(R)                                                                     |
| MOV R X     | 1    | move                 | reg(R) = reg(X)                                                                      |
| LPC R       | 1    | load program counter | reg(R) = PC                                                                          |
| LPC R+O     | 1    | load program counter | reg(R) = PC + num(O) ; where num(O) < 0x10                                           |
| LDN R N     | 1    | load nibble          | reg(R) = num(N) ; where num(N) < 0x10                                                |
| LDL R N     | 1    | load low             | reg(R) = (reg(R) & 0xF0) or num(N) ; where num(N) < 0x100                            |
| LDH R N     | 1    | load low             | reg(R) = (reg(R) & 0x0F) or num(N) << 8 ; where num(N) < 0x100                       |
| LD R N      | 2    | load                 | reg(R) = num(N) ; where num(N) < 0x10000                                             |
| LAO R L     | 2    | load address offset  | reg(R) = label("$") - label(L)                                                       |
| CALL A R    | 2    | call                 | reg(R) = label("$")+2 ; jumps to reg(A)                                              |
| CALL L A R  | 4    | call                 | reg(A) = label(L) ; reg(R) = label("$")+2 ; jumps to reg(A)                          |
| SWP X Y     | 3    | swap                 | reg(X) = reg(X) xor reg(Y) ; reg(Y) = reg(X) xor reg(Y) ; reg(X) = reg(X) xor reg(Y) |

### Jump Conditions

| ID | Name | Description       |
| -- | ---- | ----------------- |
| 00 | EQZ  | Equal to zero     |
| 01 | NEZ  | Not equal to zero |
| 02 | GTZ  | Greter than zero  |
| 03 | LTZ  | Less than zero    |
| 04 | WRY  | Register writable |
| 05 | RRY  | Register readable |
