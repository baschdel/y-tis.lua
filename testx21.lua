TisNode = require("tislib.X21")
TisDebugPrinter = require("tislib.C90")
TisFifo = require("tislib.C31")
panellib = require("tislib.utf8panel")

--This file contains a simple test setup for simple programs
--Usage:
--tis = loadfile("testtis.lua")
--tis.step() --step the program and print fancy debug output

-- programs/primes.asm
program={
	-- X21
	[0x0000]=0x400a, -- JMP 0x0a
	[0x000A]=0x3010, -- MOV NIL ACC
	[0x000B]=0x2010, -- SWP
	[0x000C]=0x0000, -- LDI 0x000
	[0x000D]=0x3613, -- MOV IMM+0x3 ACC
	[0x000E]=0x2010, -- SWP
	[0x000F]=0x31c0, -- MOV ACC ANY
	[0x0010]=0x0000, -- LDI 0x000
	[0x0011]=0x1601, -- ADD IMM+0x1
	[0x0012]=0x56fd, -- JRO -3
}

ioRoutputFifo = TisFifo(32)

outputs = {
	--[0] = TisDebugPrinter("O0: "),
	--[1] = TisDebugPrinter("O1: "),
	--[2] = TisDebugPrinter("O2: "),
	[3] = ioRoutputFifo,
}

tis = TisNode(program,{},outputs)

panel = panellib.Panel()
panel:add(tis,"TIS NODE #000",1,1,23,13)
panel:add(ioRoutputFifo,"OUTPUT #2 OF #000",24,1,23,13)


function tis.step()
	tis:tick()
	print(panel:renderUtf8())
end

print("Press Enter to single step")
local c = 1
while true do
	io.read()
	print("step: "..tostring(c))
	tis.step()
	c=c+1
end
--return tis
