-------------------------------------------
--- TIS X20 NODE ------------------------
---------------------------------------

--registers 6 and 7 are reserved
--register 0 is NIL
--registers 1-5 are 16-bit general purpose registers
--registers 8-15 are io registers

function tisNodeCanReadRegister(this,register)
	if register < 0 then return false end
	if register < 6 then return true end
	if register >= 8 and register <= 15 then
		local input = this.inputs[register&0x7]
		if input then
			return input:canRead()
		end
	end
	return false
end

function tisNodeCanWriteRegister(this,register)
	if register < 0 then return false end
	if register < 6 then return true end
	if register >= 8 and register <= 15 then
		local output = this.outputs[register&0x7]
		if output then
			return output:canWrite()
		end
	end
	return false
end

--returns value or nil
function tisNodeReadRegister(this,register)
	if register < 0 then return nil end
	if register == 0 then return 0 end
	if register < 6 then return this.registers[register] end
	if register >= 8 and register <= 15 then
		local input = this.inputs[register&0x7]
		if input then
			return (input:read()&0xFFFF)
		end
	end
	return nil
end

-- returns true on success
function tisNodeWriteRegister(this,register,value)
	value = value & 0xFFFF
	if register < 0 then return false end
	if register == 0 then return true end
	if register < 6 then this.registers[register] = value return true end
	if register >= 8 and register <= 15 then
		local output = this.outputs[register&0x7]
		if output then
			return output:write(value)
		end
	end
	return false
end

function tisNodeSetWait(this,register,write)
	this.wait = true
	tis.waitingFor = register
	tis.waitForWrite = write or false
end

function tis16BitRoll(x,a)
	local res = x<<a
	return (res&0xFFFF)|(res&0xFFFF0000)>>16
end

function tisNodeLDL(this,r,x,y)
	if not tisNodeCanReadRegister(this,r) then tisNodeSetWait(this,r) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) end
	value = tisNodeReadRegister(this,r)
	value = value & 0xFF00
	value = value | x<<4 | y
	tisNodeWriteRegister(this,r,value)
	this.pc = this.pc+1
end

function tisNodeLDH(this,r,x,y)
	if not tisNodeCanReadRegister(this,r) then tisNodeSetWait(this,r) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	value = tisNodeReadRegister(this,r)
	value = value & 0x00FF
	value = value | (x<<4 | y) <<8
	tisNodeWriteRegister(this,r,value)
	this.pc = this.pc+1
end

function tisNodeJMP(this,cond,x,t)
	--[[
		0-EQZ #equal zero
		1-NEZ #not equal zero
		2-GTZ #greater than zero
		3-LTZ #less than zero
		4-WRY #can write register t
		5-RRY #can read register t
	--]]
	if cond > 5 then tisNodeInvalidOPC(this) end
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	local incr = 1
	local xval = tisNodeReadRegister(this,x)
	if cond <= 3 then
		if not tisNodeCanReadRegister(this,t) then tisNodeSetWait(this,t) return end
		local tval = tisNodeReadRegister(this,t)
		if cond == 0 and tval == 0 then incr = xval
		elseif cond == 1 and tval ~= 0 then incr = xval
		elseif cond == 2 and tval > 0 and tval <= 0x7FFF then incr = xval
		elseif cond == 3 and tval > 0x7FFF then incr = xval
		end
	end
	if cond == 4 then
		if tisNodeCanWriteRegister(this,t) then incr = xval end
	elseif cond == 5 then
		if tisNodeCanReadRegister(this,t) then incr = xval end
	end
	if incr == 0 then this.halt = true end
	this.pc = (this.pc+incr)&0xFFFF
end

function tisNode6(this,r,x,o)
	--[[
		      r    x   o
		AJP - 0 - <x> -0- #absolute jump
		NEG <res> <x> -1-
		NOT <res> <x> -2-
		LPC <res> [o] -3- #res = PC+o #return addr
		SRF <res> [a] -4- #shift right fixed res = res>>#x
		SLF <res> [a] -5- #shift left fixed res = res<<#x
		RRF <res> [a] -6- #roll ...
		RLF <res> [a] -7- #roll ...
		LDN <res> [i] -8- #res = #i
	--]]
	if o > 8 then tisNodeInvalidOPC(this) end
	if o < 3 then
		if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	end
	if o == 0 then
		if r == 0 then -- AJP
			local xval = tisNodeReadRegister(this,x)
			this.pc = xval
			return
		else
			tisNodeInvalidOPC(this)
			return
		end
	end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	if o == 3 then -- LPC (load program counter)
		tisNodeWriteRegister(this,r,this.pc+x)
	end
	if o == 8 then -- LDI
		tisNodeWriteRegister(this,r,x)
	end
	if o >= 4 and o <= 7 then
		if not tisNodeCanReadRegister(this,r) then tisNodeSetWait(this,r) return end
		local rval = tisNodeReadRegister(this,r)
		if o == 4 then -- SRF
			tisNodeWriteRegister(this,r,rval>>x)
		elseif o == 5 then -- SLF
			tisNodeWriteRegister(this,r,rval<<x)
		elseif o == 6 then -- RRF
			tisNodeWriteRegister(this,r,tis16BitRoll(rval,-x))
		elseif o == 7 then -- RLF
			tisNodeWriteRegister(this,r,tis16BitRoll(rval,x))
		end
	end
	if o == 1 or o == 2 then
		local xval = tisNodeReadRegister(this,x)
		if o == 1 then -- NEG
			tisNodeWriteRegister(this,r,(~xval+1)&0xFFFF)
		elseif o == 2 then -- NOT
			tisNodeWriteRegister(this,r,~xval)
		end
	end
	this.pc = this.pc+1
end

function tisNodeMUL(this,r,x,y)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,y) then tisNodeSetWait(this,y) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local res = tisNodeReadRegister(this,x)*tisNodeReadRegister(this,y)
	tisNodeWriteRegister(this,r,res&0xFFFF)
	this.pc = this.pc+1
end

function tisNodeDIV(this,r,x,y)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,y) then tisNodeSetWait(this,y) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local yval = tisNodeReadRegister(this,y)
	local xval = tisNodeReadRegister(this,x)
	if yval == 0 then
		tisNodeWriteRegister(this,r,0xFFFF)
	else
		tisNodeWriteRegister(this,r,xval//yval)
	end
	this.pc = this.pc+1
end

function tisNodeAND(this,r,x,y)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,y) then tisNodeSetWait(this,y) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local res = tisNodeReadRegister(this,x)&tisNodeReadRegister(this,y)
	tisNodeWriteRegister(this,r,res)
	this.pc = this.pc+1
end

function tisNodeOR(this,r,x,y)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,y) then tisNodeSetWait(this,y) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local res = tisNodeReadRegister(this,x)|tisNodeReadRegister(this,y)
	tisNodeWriteRegister(this,r,res)
	this.pc = this.pc+1
end

function tisNodeXOR(this,r,x,y)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,y) then tisNodeSetWait(this,y) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local res = tisNodeReadRegister(this,x)~tisNodeReadRegister(this,y)
	tisNodeWriteRegister(this,r,res)
	this.pc = this.pc+1
end

function tisNodeSHF(this,r,x,a)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,a) then tisNodeSetWait(this,a) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local av = tisNodeReadRegister(this,a)
	if av > 0x7FFF then av = (~av+1)&0xFFFF end
	local res = tisNodeReadRegister(this,x)<<av
	tisNodeWriteRegister(this,r,res)
	this.pc = this.pc+1
end

function tisNodeROL(this,r,x,a)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,a) then tisNodeSetWait(this,a) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local av = tisNodeReadRegister(this,a)
	if av > 0x7FFF then av = -((~av+1)&0xF) else av = av&0xF end
	local res = tis16BitRoll(tisNodeReadRegister(this,x),av)
	tisNodeWriteRegister(this,r,res)
	this.pc = this.pc+1
end

function tisNodeSUB(this,r,x,y)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,y) then tisNodeSetWait(this,y) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local res = tisNodeReadRegister(this,x)-tisNodeReadRegister(this,y)
	tisNodeWriteRegister(this,r,res&0xFFFF)
	this.pc = this.pc+1
end

function tisNodeADD(this,r,x,y)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanReadRegister(this,y) then tisNodeSetWait(this,y) return end
	if not tisNodeCanWriteRegister(this,r) then tisNodeSetWait(this,r,true) return end
	local res = tisNodeReadRegister(this,x)+tisNodeReadRegister(this,y)
	tisNodeWriteRegister(this,r,res&0xFFFF)
	this.pc = this.pc+1
end

function tisNodeInvalidOPC(this,...)
	this.halt = true
	this.invalidOpc = true
end

local tisNodeOpcodeFunctionTable = {
	[0x0] = tisNodeInvalidOPC,
	[0x1] = tisNodeInvalidOPC,
	[0x2] = tisNodeInvalidOPC,
	[0x3] = tisNodeLDL,
	[0x4] = tisNodeLDH,
	[0x5] = tisNodeJMP,
	[0x6] = tisNode6,
	[0x7] = tisNodeMUL,
	[0x8] = tisNodeDIV,
	[0x9] = tisNodeAND,
	[0xA] = tisNodeOR,
	[0xB] = tisNodeXOR,
	[0xC] = tisNodeSHF,
	[0xD] = tisNodeROL,
	[0xE] = tisNodeSUB,
	[0xF] = tisNodeADD,
}

function tisNodeTick(this)
	if this.halt then return end
	if not this.wait then
		this.ir = this.program[this.pc] or 0x6000 --AJMP NIL
	end
	this.wait = false
	local opc = (this.ir & 0xF000) >> 12
	local r = (this.ir & 0x0F00) >> 8
	local x = (this.ir & 0x00F0) >> 4
	local y = this.ir & 0x000F
	local func = tisNodeOpcodeFunctionTable[opc]
	func(this,r,x,y)
end

function tisNodeReset(this)
	this.wait = false
	this.halt = false
	this.invalidOpc = false
	this.waitingFor = 0
	this.waitForWrite = false
	this.ir = 0
	this.pc = 0
	tis.registers = {0,0,0,0,0}
end

function tisNodeRenderUtf8(this,...)
	local tisNodeRenderUtf8 = require("tislib.X20_toUtf8")
	this.renderUtf8 = tisNodeRenderUtf8
	return tisNodeRenderUtf8(this,...)
end

--creates a tis node
function TisNode(program,inputs,outputs)
	local tis = {}
	tis.type = "X20"
	tis.registers = {0,0,0,0,0}
	tis.program = program or {}
	tis.pc = 0 --program counter (stores next instruction address)
	tis.wait = false
	tis.halt = false
	tis.invalidOpc = false
	tis.waitingFor = 0
	tis.waitForWrite = false
	tis.ir = 0 --instruction register
	tis.inputs = inputs or {}
	tis.outputs = outputs or {}
	tis.tick = tisNodeTick
	tis.reset = tisNodeReset
	tis.renderUtf8 = tisNodeRenderUtf8
	return tis
end

return TisNode
