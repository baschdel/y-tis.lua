local function splitLines(str)
	local lines = {}
	local offset = 1
	local line = 1
	while true do
		local pos = string.find(str,"\n",offset,true)
		if not pos then lines[line] =  str:sub(offset) break end
		lines[line] = str:sub(offset,pos-1)
		offset = pos+1
		line = line+1
	end
	return lines
end

local function formatLines(lines,width,height)
	local out = {}
	if not height then
		height = 0
		for i,_ in pairs(lines) do
			if i>height then height=i end
		end
	end
	for i=1,height do
		local l = lines[i] or ""
		if width then
			out[i] = l:sub(1,(utf8.offset(l,width+1) or 0)-1)..(" "):rep(width-utf8.len(l))
		else
			out[i] = l
		end
	end
	return lines
end

local function insertString(buffer,str,offset)
	local bufflen = utf8.len(buffer)
	if offset > bufflen+1 then
		buffer=buffer..(" "):rep(offset-bufflen-1)
	end
	if offset == utf8.len(buffer) then
		return buffer..str
	end
	local s = buffer:sub(1,utf8.offset(buffer,offset)-1)
	local ie = utf8.offset(buffer,offset+utf8.len(str))
	local e = ""
	if ie then
		e = buffer:sub(ie)
	end
	return s..str..e
end

local function utf8PanelAdd(this,object,arg,x,y,w,h)
	local id = #this.objects+1
	this.objects[id] = {
		x=x,y=y,
		w=w,h=h,
		arg=arg,
		o=object
	}
	return id
end

local function utf8PanelRemove(this,id)
	this.objects[id] = nil
end

local function utf8PanelRender(this,_,width,height)
	local lines = {}
	for _,obj in pairs(this.objects) do
		local str = obj.o
		if type(str) == "table" then
			str = str:renderUtf8(obj.arg,obj.w,obj.h)
		end
		local olines = splitLines(str or "")
		olines = formatLines(olines,obj.w,obj.h)
		for i=1,obj.h do
			lines[obj.y+i] = insertString(lines[obj.y+i] or "",olines[i] or "",obj.x)
		end
	end
	lines = formatLines(lines,width,height)
	out = ""
	for i=1,#lines do
		out=out..(lines[i] or "").."\n"
	end
	return out
end

local function Utf8Panel()
	this = {}
	this.objects = {}
	this.add = utf8PanelAdd
	this.renderUtf8 = utf8PanelRender
	return this
end

return {
	Panel = Utf8Panel
}
