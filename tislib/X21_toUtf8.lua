local canLoadDisassembler,disassembler = pcall(require,"tislib.X21disassembler")
if not canLoadDisassembler then disassembler = nil end

local registers = {
	[0] = "NIL",
	[1] = "ACC",
	[6] = "IMM",
	[8] = "UP",
	[9] = "LEFT",
	[10] = "DOWN",
	[11] = "RIGHT",
	[12] = "ANY",
	[13] = "LAST",
}

-- width: 24
-- height: 13

function tisNodeRenderUtf8(this,title)
	local status_message = "OK"
	if this.halt then
		if this.invalidOpc then status_message = "INVALID OPCODE" 
		else status_message = "HALT" end
	elseif this.wait then
		status_message = "WAITING FOR "
		if registers[this.waitingFor] then
			status_message = ("%s %-5s"):format(status_message,registers[this.waitingFor])
		else
			status_message = status_message.."R"..tostring(waitingFor).."    "
		end
		if this.waitForWrite then
			status_message = status_message.."<"
		else
			status_message = status_message..">"
		end
	elseif disassembler then
		status_message = "> "..disassembler.disassembleSingleInstruction(this.ir)
	end
	ioflags = {}
	for i=8,12 do
		local f = ""
		if tisNodeCanReadRegister(this,i)  then f = "R" else f = " " end
		if tisNodeCanWriteRegister(this,i) then f = f.."W" else f = f.." " end
		ioflags[i] = f
	end
	
	out =      string.format("┏━━━━━━━━━━━━━━━━━━━━━┓\n")
	out = out..string.format("┃ %-19s ┃\n",title:sub(1,19))
	out = out..string.format("┃ %-19s ┃\n",status_message)
	out = out..string.format("┣━━━━━━━━━━━━┳━━━━━━━━┫\n")
	out = out..string.format("┃ ACC 0x%04x ┃ UP  %s ┃\n",this.acc,ioflags[8])
	out = out..string.format("┃ BAK 0x%04x ┃ DWN %s ┃\n",this.bak,ioflags[9])
	out = out..string.format("┣━━━━━━━━━━━━┫ LFT %s ┃\n",ioflags[10])
	out = out..string.format("┃ PC  0x%02x   ┃ RGT %s ┃\n",this.pc,ioflags[11])
	out = out..string.format("┃ IR  0x%04x ┃ LAST:  ┃\n",this.ir)
	out = out..string.format("┃ IMM 0x%04x ┃  %-5s ┃\n",this.imm,(registers[(this.last or -10)+9] or "NONE"))
	out = out..string.format("┗━━━━━━━━━━━━┻━━━━━━━━┛\n")
	return out
end

return tisNodeRenderUtf8

--[[
	┏━━━━━━━━━━━━━━━━━━━━━┓
	┃ TIS NODE #001       ┃
	┃ WAITING FOR RIGHT   ┃
	┣━━━━━━━━━━━━┳━━━━━━━━┫
	┃ ACC 0x0000 ┃ UP  %s ┃
	┃ BAK 0x0000 ┃ DWN %s ┃
	┣━━━━━━━━━━━━┫ LFT %s ┃
	┃ PC  0x00   ┃ LGT %s ┃
	┃ IR  0x0000 ┃ LAST:  ┃
	┃ IMM 0x0000 ┃  %s    ┃
	┗━━━━━━━━━━━━┻━━━━━━━━┛
--]]
