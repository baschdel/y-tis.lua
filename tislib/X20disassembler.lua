
--[[
Instructions:
#|
0|
1|
2|
3|LDL <res> [value]
4|LDH <res> [value]
5|JMP [con] <x> <t> #conditional relative jump always reads the value in x
 |  0-EQZ #equal zero
 |  1-NEZ #not equal zero
 |  2-GTZ #greater than zero
 |  3-LTZ #less than zero
 |  4-WRY #can write register t
 |  5-RRY #can read register t
6|AJP - 0 - <x> -0- #absolute jump
 |NEG <res> <x> -1-
 |NOT <res> <x> -2-
 |LPC <res> [o] -3- #res = PC+o #return addr
 |SRF <res> [a] -4- #shift right fixed res = res>>#x
 |SLF <res> [a] -5- #shift left fixed res = res<<#x
 |RRF <res> [a] -6- #roll ...
 |RLF <res> [a] -7- #roll ...
 |LDN <res> [i] -8- #res = #i
7|MUL <res> <x> <y> #unsigned multiplication
8|DIV <res> <x> <y> #unsigned integer division
9|AND <res> <x> <y>
A|OR  <res> <x> <y>
B|XOR <res> <x> <y>
C|SHF <res> <x> <a> # x<<a if a<0 x>>-a
D|ROL <res> <x> <a> # same as SHF
E|SUB <res> <x> <y>
F|ADD <res> <x> <y>
--]]

local registers = {
	[0] = "NIL",
	[1] = "ACC",
	[2] = "R2",
	[3] = "R3",
	[4] = "R4",
	[5] = "R5",
	[6] = "/6",
	[7] = "/7",
	[8]  = "IO0",
	[9]  = "IO1",
	[10] = "IO2",
	[11] = "IO3",
	[12] = "IO4",
	[13] = "IO5",
	[14] = "IO6",
	[15] = "IO7",
}

local jumpconditions = {
	[0] = "EQZ",
	[1] = "NEZ",
	[2] = "LTZ",
	[3] = "GTZ",
	[4] = "WRY",
	[5] = "RRY",
	[6] = "/6",
	[7] = "/7",
	[8] = "/8",
	[9] = "/9",
	[10] = "/10",
	[11] = "/11",
	[12] = "/12",
	[13] = "/13",
	[14] = "/14",
	[15] = "/15",
}

function ijmp(instruction)
	local c = jumpconditions[(instruction&0x0F00)>>8]
	local a = registers[(instruction&0x00F0)>>4]
	local t = registers[(instruction&0x000F)]
	if t == "NIL" and c == "EQZ" then
		if a == "NIL" then
			return "HLT"
		else
			return string.format("RJMP %s",a)
		end
	else
		return string.format("JMP %s %s %s",t,c,a)
	end
end

function iajmp(instruction)
	local a = registers[(instruction&0x00F0)>>4]
	return string.format("AJMP %s",a)
end

function ildn(instruction)
	local r = registers[(instruction&0x0F00)>>8]
	local n = ((instruction&0x00F0)>>4)
	return string.format("LDN %s 0x%01x",r,n)
end

function imov(instruction)
	local r = registers[(instruction&0x0F00)>>8]
	local x = registers[(instruction&0x00F0)>>4]
	return string.format("MOV %s %s",r,x)
end

function ildl(instruction)
	local r = registers[(instruction&0x0F00)>>8]
	local x = (instruction&0x00FF)
	return string.format("LDL %s 0x%02x",r,x)
end

function ildh(instruction)
	local r = registers[(instruction&0x0F00)>>8]
	local x = (instruction&0x00FF)
	return string.format("LDH %s 0x%02x",r,x)
end

function a2(instruction,command)
	local r = registers[(instruction&0x0F00)>>8]
	local x = registers[(instruction&0x00F0)>>4]
	if r == x then
		return string.format("%s %s",command,r)
	else
		return string.format("%s %s %s",command,r,x)
	end
end

function a2n(instruction,command)
	local r = registers[(instruction&0x0F00)>>8]
	local n = (instruction&0x00F0)>>4
	return string.format("%s %s 0x%01x",command,r,n)
end

function a3(instruction,command)
	local r = registers[(instruction&0x0F00)>>8]
	local x = registers[(instruction&0x00F0)>>4]
	local y = registers[(instruction&0x000F)]
	if r == x then
		return string.format("%s %s %s",command,r,y)
	else
		return string.format("%s %s %s %s",command,r,x,y)
	end
end

function iadd(instruction) return a3(instruction,"ADD") end
function isub(instruction) return a3(instruction,"SUB") end
function imul(instruction) return a3(instruction,"MUL") end
function idiv(instruction) return a3(instruction,"DIV") end
function ishf(instruction) return a3(instruction,"SHF") end
function irol(instruction) return a3(instruction,"ROL") end
function iand(instruction) return a3(instruction,"AND") end
function ior(instruction)  return a3(instruction,"OR")  end
function ixor(instruction) return a3(instruction,"XOR") end

function inot(instruction) return a2(instruction,"NOT") end
function ineg(instruction) return a2(instruction,"NEG") end

function ilpc(instruction)
	local r = registers[(instruction&0x0F00)>>8]
	local n = (instruction&0x00F0)>>4
	if n==0 then
		return string.format("LPC %s",r)
	else
		return string.format("LPC %s 0x%01x",r,n)
	end
end

function isrf(instruction) return a2n(instruction,"SRF") end
function islf(instruction) return a2n(instruction,"SLF") end
function irrf(instruction) return a2n(instruction,"RRF") end
function irlf(instruction) return a2n(instruction,"RLF") end

local decoders = {
	--{mask,match,callback},
	{0xFFFF,0xF000,"NOP"},
	{0xFFFF,0x5000,"HLT"},
	{0xF00F,0xF000,imov},
	{0xF000,0xF000,iadd},
	{0xF000,0xE000,isub},
	{0xF000,0x7000,imul},
	{0xF000,0x8000,idiv},
	{0xF000,0xC000,ishf},
	{0xF000,0xD000,irol},
	{0xF000,0x9000,iand},
	{0xF000,0xA000,ior},
	{0xF000,0xB000,ixor},
	{0xF000,0x3000,ildl},
	{0xF000,0x4000,ildh},
	{0xF000,0x5000,ijmp},
	{0xFF0F,0x6000,iajmp},
	{0xF00F,0x6001,ineg},
	{0xF00F,0x6002,inot},
	{0xF00F,0x6003,ilpc},
	{0xF00F,0x6004,isrf},
	{0xF00F,0x6005,islf},
	{0xF00F,0x6006,irrf},
	{0xF00F,0x6007,irlf},
	{0xF00F,0x6008,ildn},
}

function disassembleSingleInstruction(instruction)
	for _,v in pairs(decoders) do
		if instruction & v[1] == v[2] then
			if type(v[3]) == "string" then
				return v[3]
			else
				return v[3](instruction)
			end		
		end
	end
	return string.format("INST /0x%04x",instruction)
end

return {disassembleSingleInstruction = disassembleSingleInstruction}
