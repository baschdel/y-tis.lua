
-- will draw the the content of the Connector in the specified space
-- width will be clamped to a minimum of 10
-- height will be clamped to a minimum of 10
function ConnectorToUtf8(this,title,width,height)
	if height == nil then height = 13 end
	if width == nil then width = 23 end
	if height < 4 then height = 4 end
	if width < 10 then width = 10 end
	local out = string.format("┏%s┓\n",("━"):rep(width-2))
	local fheader = "┃ %-"..tostring(width-4).."s ┃\n"
	out = out..string.format(fheader,title:sub(1,width-4))
	local valuecount = this:getValueCount()
	local status = tostring(valuecount)
	local valuemax = tostring(this.size)
	if status:len()+valuemax:len()+1 <= width-4 then
		status = status.."/"..valuemax	
	end
	if status:len()+3+this.type:len() <= width-4 then
		status = status..((" "):rep(width-4-status:len()-this.type:len()-2)).."["..this.type.."]"
	end
	out = out..string.format(fheader,status)
	local rows = height-5
	local colums = (width-2)//8
	if rows >= 0 then out = out..string.format("┣%s┫\n",("━"):rep(width-2)) end
	if rows > 0 then
		local dval = rows * colums --displayed values
		local dots = false -- on which displayed value the ... will be placed
		if dval < valuecount then
			dots = dval // 2
		end
		local cvs = {}
		for i=1,(dots or dval) do
			cvs[i] = this.storage[i]
		end
		if dots then
			local doff = valuecount-dval
			for i=dots,dval do
				cvs[i] = this.storage[doff+i]
			end
			cvs[dots] = "..."
		end
		--render cvs
		for r=1,rows do
			local line = ""
			for c=1,colums do
				local val = cvs[(c-1)*rows+r]
				--local val = tostring((c-1)*rows+r)
				--local val = tostring(r)..","..tostring(c)
				if type(val) == "number" then
					line = line..string.format(" 0x%04x ",val)
				elseif type(val) == "string" then
					line = line..string.format(" %-6s ",val:sub(1,6))
				else
					line = line.."        "
				end
			end
			out = out..string.format("┃%s%s┃\n",line,(" "):rep(width-line:len()-2))
		end
	end
	out = out..string.format("┗%s┛\n",("━"):rep(width-2))
	return out	
end

return ConnectorToUtf8

--[[
┏━━━━━━━━━━━━━━━━┓
┃ Stack #001     ┃
┃ 31/128   [C30] ┃
┣━━━━━━━━━━━━━━━━┫
┃ 0x0000  0x0000 ┃
┃ 0x0000  0x0000 ┃
┃ 0x0000  0x0000 ┃
┃ 0x0000  0x0000 ┃
┃ 0x0000  0x0000 ┃
┃ 0x0000  0x0000 ┃
┃ 0x0000  0x0000 ┃
┃ 0x0000  0x0000 ┃
┃ ...     0x0000 ┃
┗━━━━━━━━━━━━━━━━┛
]]--
