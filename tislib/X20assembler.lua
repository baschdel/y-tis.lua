local assemblerlib = require("tislib.assembler")
local Assembler = assemblerlib.Assembler
local toNumber = assemblerlib.toNumber
local encodeRegOp = assemblerlib.encodeRegOp
local registerOrError = assemblerlib.registerOrError
local getNumberOrSymbolOrError = assemblerlib.getNumberOrSymbolOrError

function printDummy() end
local print = printDummy --disable debug output

local X20registers = {
	NIL=0,
	R1=1, ACC=1,
	R2=2,
	R3=3,
	R4=4,
	R5=5,
	I0=8, O0=8,
	I1=9, O1=9,
	I2=10, O2=10,
	I3=11, O3=11,
	I4=12, O4=12,
	I5=13, O5=13,
	I6=14, O6=14,
	I7=15, O7=15,
}

local jumpconditions = {
	EQZ=0,
	NEZ=1,
	GTZ=2,
	LTZ=3,
	WRY=4,
	RRY=5
}

function getJumpCondition(token)
	if token.sub(1,1) == "/" then
		local cond = toNumber(token:sub(2))
		if (not cond) or (cond > 0xF) then return nil end
		return cond
	end
	return jumpconditions[token:upper()]
end

function a3MUL(this,r,x,y) return encodeRegOp(this,r,x,y,0x7) end
function a3DIV(this,r,x,y) return encodeRegOp(this,r,x,y,0x8) end
function a3AND(this,r,x,y) return encodeRegOp(this,r,x,y,0x9) end
function a3OR(this,r,x,y)  return encodeRegOp(this,r,x,y,0xA) end
function a3XOR(this,r,x,y) return encodeRegOp(this,r,x,y,0xB) end
function a3SHF(this,r,x,y) return encodeRegOp(this,r,x,y,0xC) end
function a3ROL(this,r,x,y) return encodeRegOp(this,r,x,y,0xD) end
function a3SUB(this,r,x,y) return encodeRegOp(this,r,x,y,0xE) end
function a3ADD(this,r,x,y) return encodeRegOp(this,r,x,y,0xF) end

function a2MUL(this,r,x) return encodeRegOp(this,r,r,x,0x7) end
function a2DIV(this,r,x) return encodeRegOp(this,r,r,x,0x8) end
function a2AND(this,r,x) return encodeRegOp(this,r,r,x,0x9) end
function a2OR(this,r,x)  return encodeRegOp(this,r,r,x,0xA) end
function a2XOR(this,r,x) return encodeRegOp(this,r,r,x,0xB) end
function a2SHF(this,r,x) return encodeRegOp(this,r,r,x,0xC) end
function a2ROL(this,r,x) return encodeRegOp(this,r,r,x,0xD) end
function a2SUB(this,r,x) return encodeRegOp(this,r,r,x,0xE) end
function a2ADD(this,r,x) return encodeRegOp(this,r,r,x,0xF) end

function a2MOV(this,r,x) return a3ADD(this,r,x,"NIL") end
function a0NOP(this) return a3ADD(this,"NIL","NIL","NIL") end

function a3JMP(this,t,c,x)
	local cond = getJumpCondition(c)
	local xreg = registerOrError(this,x)
	local treg = registerOrError(this,t)
	if not cond then error("No such jump condition: "..tostring(r)) end
	local op = 0x5000
	op = op | cond<<8
	op = op | xreg<<4
	op = op | treg
	return {op} 
end

function a1AJMP(this,x)
	print("a1AJMP(this,"..tostring(x)..")")
	local xreg = registerOrError(this,x)
	local op = 0x6000
	op = op | xreg<<4
	return {op}
end

function a1RJP(this,x)
	local xreg = registerOrError(this,x)
	local op = 0x5000
	op = op | xreg<<4
	return {op}
end

function encode6OpX(this,res,x,f)
	local rreg = registerOrError(this,res)
	local xreg = registerOrError(this,x)
	local op = 0x6000
	op = op | rreg<<8
	op = op | xreg<<4
	op = op | f
	return {op}
end

function a2NEG(this,r,x) return encode6OpX(this,r,x,1) end
function a2NOT(this,r,x) return encode6OpX(this,r,x,2) end

function a1NEG(this,r) return encode6OpX(this,r,r,1) end
function a1NOT(this,r) return encode6OpX(this,r,r,2) end

function encode6OpN(this,res,n,f)
	print("encode6OpN(this,"..tostring(res)..","..tostring(n)..","..tostring(f)..")")
	n = getNumberOrSymbolOrError(this,n)
	local rreg = registerOrError(this,res)
	if n > 0xF then error("Constant too large") end
	local op = 0x6000
	op = op | rreg<<8
	op = op | n<<4
	op = op | f
	return {op}
end

function a2LPC(this,r,n) return encode6OpN(this,r,n,3) end
function a2SRF(this,r,n) return encode6OpN(this,r,n,4) end
function a2SLF(this,r,n) return encode6OpN(this,r,n,5) end
function a2RLF(this,r,n) return encode6OpN(this,r,n,6) end
function a2RRF(this,r,n) return encode6OpN(this,r,n,7) end
function a2LDN(this,r,n) return encode6OpN(this,r,n,8) end

function a1LPC(this,r) return encode6OpN(r,0,3) end

function encodeLoad8Bit(this,res,n,f)
	print("encodeLoad8Bit(this,"..tostring(res)..","..tostring(n)..","..tostring(f)..")")
	local rreg = registerOrError(this,res)
	if n > 0xFF then error("Constant too large") end
	local op = f<<12
	op = op | rreg<<8
	op = op | n
	return {op}
end

function a2LDL(this,r,n) return encodeLoad8Bit(this,r,n,3) end
function a2LDH(this,r,n) return encodeLoad8Bit(this,r,n,4) end

function encodeLoadConstant(this,r,const)
	print("encodeLoadConstant("..tostring(r)..","..tostring(const)..")")
	if const <= 0xFFFF then
		local op1 = a2LDH(this,r,(const&0xFF00)>>8)[1]
		local op2 = a2LDL(this,r,const&0xFF)[1]
		return {op1,op2}
	else
		error("Constant too large")
	end
end

function a2LD(this,r,num)
	print("a2LD(this,"..tostring(r)..","..tostring(num)..")")
	local n = getNumberOrSymbolOrError(this,num)
	return encodeLoadConstant(this,r,n)
end

--Load address offset
function a2LAO(this,r,addr,off)
	local n = getNumberOrSymbolOrError(this,addr)
	n = n-this.symbols.labels["$"]+(off or 0)
	return encodeLoadConstant(this,r,n)
end

function a2AJMP(this,addr,x)
	print("a2AJMP(this,"..tostring(addr)..","..tostring(x)..")")
	local op12 = a2LD(this,x,addr)
	local op3 = a1AJMP(this,x)
	return {op12[1],op12[2],op3[1]}
end

function a2RJP(this,addr,x)
	local n = getNumberOrSymbolOrError(this,addr)
	n = n-this.symbols.labels["$"]-2
	local ops = encodeLoadConstant(this,x,n)
	ops[3] = a1RJP(this,x)[1]
	return ops
end

function a0HLT(this)
	return a1RJP(this,"NIL")
end

function a4JMP(this,t,c,addr,x)
	local ops = a2LAO(this,x,addr,-2)
	ops[3] = a3JMP(this,t,c,x)[1]
	return ops
end

function a2CALL(this,x,ret)
	local ops = a2LPC(this,ret,2)
	ops[2] = a1AJMP(this,x)[1]
	return ops
end

function a3CALL(this,addr,x,ret)
	local op1 = a2LPC(this,ret,4)
	local op23 = a2LD(this,x,addr)
	local op4 = a1AJMP(this,x)
	return {op1[1],op23[1],op23[2],op4[1]}
end

--https://en.wikipedia.org/wiki/XOR_swap_algorithm for an explanaition
function a2SWP(this,x,y)
	print("s2SWP(this,"..tostring(x)..","..tostring(y)..")")
	local op1 = a2XOR(this,x,y)[1]
	local op2 = a2XOR(this,y,x)[1]
	local op3 = a2XOR(this,x,y)[1]
	return {op1,op2,op3}
end

function TisX20Assembler()
	local assembler = Assembler()
	assembler.registers = X20registers
	assembler:addPattern("NOP",a0NOP,1) --do nothing
	assembler:addPattern("ADD ~ ~ ~",a3ADD,1) -- <1> = <2> + <3>
	assembler:addPattern("SUB ~ ~ ~",a3SUB,1) -- <1> = <2> - <3>
	assembler:addPattern("MUL ~ ~ ~",a3MUL,1) -- <1> = <2> * <3>
	assembler:addPattern("DIV ~ ~ ~",a3DIV,1) -- <1> = <2> / <3>
	assembler:addPattern("AND ~ ~ ~",a3AND,1) -- <1> = <2> & <3>
	assembler:addPattern("OR ~ ~ ~",a3OR,1)   -- <1> = <2> | <3>
	assembler:addPattern("XOR ~ ~ ~",a3XOR,1) -- <1> = <2> ^ <3>
	assembler:addPattern("SHF ~ ~ ~",a3SHF,1) -- <1> = <2> << <3>
	assembler:addPattern("ROL ~ ~ ~",a3ROL,1) -- <1> = <2> ROLL <3>
	assembler:addPattern("ADD ~ ~",a2ADD,1) -- <1> = <1> + <2>
	assembler:addPattern("SUB ~ ~",a2SUB,1) -- <1> = <1> - <2>
	assembler:addPattern("MUL ~ ~",a2MUL,1) -- <1> = <1> * <2>
	assembler:addPattern("DIV ~ ~",a2DIV,1) -- <1> = <1> / <2>
	assembler:addPattern("AND ~ ~",a2AND,1) -- <1> = <1> & <2>
	assembler:addPattern("OR ~ ~",a2OR,1)   -- <1> = <1> | <2>
	assembler:addPattern("XOR ~ ~",a2XOR,1) -- <1> = <1> ^ <2>
	assembler:addPattern("SHF ~ ~",a2SHF,1) -- <1> = <1> << <2>
	assembler:addPattern("ROL ~ ~",a2ROL,1) -- <1> = <1> -ROL <2>
	assembler:addPattern("JMP ~ ~ ~",a3JMP,1) -- if <1> meets condition [2] add <3> to pc
	assembler:addPattern("JMP ~ ~ ~ ~",a4JMP,3) -- if <1> meets condition [2] jump to [3] using <4> to store the offset
	assembler:addPattern("AJMP ~",a1AJMP,1) -- jump to address stored in <1>
	assembler:addPattern("AJMP ~ ~",a2AJMP,3) -- jump to [1] using <2> to store the address
	assembler:addPattern("RJMP ~",a1RJP,1) -- add <1> to pc
	assembler:addPattern("RJMP ~ ~",a2RJP,3) -- jump to [1] using <2> to store the offset
	assembler:addPattern("NEG ~ ~",a2NEG,1) -- <1> = -<2>
	assembler:addPattern("NOT ~ ~",a2NOT,1) -- <1> = ~<2>
	assembler:addPattern("NEG ~",a1NEG,1) -- <1> = -<1>
	assembler:addPattern("NOT ~",a1NOT,1) -- <1> = ~<1>
	assembler:addPattern("LPC ~",a1LPC,1) -- <1> = pc
	assembler:addPattern("LPC ~ ~",a2LPC,1) -- <1> = pc + [2] ;where [2] < 0x10
	assembler:addPattern("LDN ~ ~",a2LDN,1) -- <1> = [2] ;where [2] < 0x10
	assembler:addPattern("LDL ~ ~",a2LDL,1) -- <1> = (<1> & 0xF0) | [2]
	assembler:addPattern("LDH ~ ~",a2LDH,1) -- <1> = (<1> & 0x0F) | [2] << 8
	assembler:addPattern("LD ~ ~",a2LD,2) -- <1> = [2] ;1 instruction if [2] < 0x10 else 2 instructions
	assembler:addPattern("MOV ~ ~",a2MOV,1) -- <1> = [2]
	assembler:addPattern("LAO ~ ~",a2MOV,2) -- <1> = offset_to([2])
	assembler:addPattern("HLT",a0HLT,1) -- halt
	assembler:addPattern("CALL ~ ~",a2CALL,2) -- store return address in <2> and jump to the addess stored in <1>
	assembler:addPattern("CALL ~ ~ ~",a3CALL,4) -- store return address in <3> and jump to [1] using <2> to store the destinatin address (if <3> and <2> are io ports connected to the same stack this will have no problem)
	assembler:addPattern("SWP ~ ~",a2SWP,3) --swaps the contents of <1> and <2> using the xor swap algorythm ;<1> will be written twice <1> and <2> qill be read the times each
	return assembler
end

return TisX20Assembler
