;A simple program to test if the assembler is working
;should output 0;3;1;4;2;5;...
#DEFINE OUTPUT O2
AJMP START ACC

#ORG 0xA
START:
	LDN ACC 1
	LDN R2  0
	LDN R3  3
LOOP:
	MOV OUTPUT R2
	ADD R2 ACC
	SWP R2 R3
	RJMP LOOP R5

