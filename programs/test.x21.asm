;A simple program to test if the assembler is working
;should output 0;3;1;4;2;5;...
#DEFINE OUTPUT ANY
JMP START

#ORG 0xA
START:
	MOV 0 ACC
	SWP
	MOV 3 ACC
LOOP:
	SWP
	MOV ACC OUTPUT
	ADD 1
	JRO -3
	
