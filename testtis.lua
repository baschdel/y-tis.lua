TisNode = require("tislib.X20")
TisDebugPrinter = require("tislib.C90")
TisFifo = require("tislib.C31")
panellib = require("tislib.utf8panel")

--This file contains a simple test setup for simple programs
--Usage:
--tis = loadfile("testtis.lua")
--tis.step() --step the program and print fancy debug output

-- programs/primes.asm
program={
	[0x0000]=0x6a18, -- LDN IO2 0x1
	[0x0001]=0x6a28, -- LDN IO2 0x2
	[0x0002]=0x6118, -- LDN ACC 0x1
	[0x0003]=0x6228, -- LDN R2 0x2
	[0x0004]=0xf112, -- ADD ACC R2
	[0x0005]=0x6543, -- LPC R5+0x4
	[0x0006]=0x4400, -- LDH R4 0x00
	[0x0007]=0x3410, -- LDL R4 0x10
	[0x0008]=0x6040, -- AJMP R4
	[0x0009]=0x44ff, -- LDH R4 0xff
	[0x000A]=0x34f8, -- LDL R4 0xf8
	[0x000B]=0x5042, -- JMP R2 EQZ R4
	[0x000C]=0xfa10, -- MOV IO2 ACC
	[0x000D]=0x44ff, -- LDH R4 0xff
	[0x000E]=0x34f4, -- LDL R4 0xf4
	[0x000F]=0x5040, -- RJMP R4
	[0x0010]=0xf310, -- MOV R3 ACC
	[0x0011]=0x6218, -- LDN R2 0x1
	[0x0012]=0xe332, -- SUB R3 R2
	[0x0013]=0xe332, -- SUB R3 R2
	[0x0014]=0x4400, -- LDH R4 0x00
	[0x0015]=0x340b, -- LDL R4 0x0b
	[0x0016]=0x5043, -- JMP R3 EQZ R4
	[0x0017]=0xf332, -- ADD R3 R2
	[0x0018]=0x8413, -- DIV R4 ACC R3
	[0x0019]=0x7443, -- MUL R4 R3
	[0x001A]=0xe441, -- SUB R4 ACC
	[0x001B]=0x4200, -- LDH R2 0x00
	[0x001C]=0x3206, -- LDL R2 0x06
	[0x001D]=0x5024, -- JMP R4 EQZ R2
	[0x001E]=0x44ff, -- LDH R4 0xff
	[0x001F]=0x34f1, -- LDL R4 0xf1
	[0x0020]=0x5040, -- RJMP R4
	[0x0021]=0x6218, -- LDN R2 0x1
	[0x0022]=0x6050, -- AJMP R5
	[0x0023]=0x6208, -- LDN R2 0x0
	[0x0024]=0x6050, -- AJMP R5
}

io2outputFifo = TisFifo(32)

outputs = {
	[0] = TisDebugPrinter("O0: "),
	[1] = TisDebugPrinter("O1: "),
	[2] = io2outputFifo,
	[3] = TisDebugPrinter("O3: "),
	[4] = TisDebugPrinter("O4: "),
	[5] = TisDebugPrinter("O5: "),
	[6] = TisDebugPrinter("O6: "),
	[7] = TisDebugPrinter("O7: "),
}

tis = TisNode(program,{},outputs)

panel = panellib.Panel()
panel:add(tis,"TIS NODE #000",1,1,23,13)
panel:add(io2outputFifo,"OUTPUT #2 OF #000",24,1,23,13)


function tis.step()
	tis:tick()
	print(panel:renderUtf8())
end

print("Press Enter to single step")
local c = 1
while true do
	io.read()
	print("step: "..tostring(c))
	tis.step()
	c=c+1
end
--return tis
